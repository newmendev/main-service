import os

import requests
from rest_framework import generics, status
from rest_framework.response import Response

from feedback_app.serializers import FedbackSerializer
from django.core.mail import EmailMultiAlternatives
import json

class SendFeedback(generics.GenericAPIView):
    """ оправляем тип и текст отзыва на почту """
    serializer_class = FedbackSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data
        name = data["name"]
        phone = data["phone"]
        message = data["message"]
        body = f'Имя: {name} <BR> Телефон: {phone} <BR> Сообщение: {message}'

        send_to = os.environ.get("SEND_TO_FEED_BACK")
        if not send_to:
            raise ValueError(f"environment SEND_TO_FEED_BACK has empty email")

        message = EmailMultiAlternatives(
            subject='SVOE PORTAL',
            body=body,
            to=[send_to],
            from_email=os.environ["EMAIL_HOST_USER"]
        )
        message.send()

        return Response()


class VerifyCaptcha(generics.GenericAPIView):
    def post(self, request):
        data = request.data
        body = {
            'secret': '6LdmodwbAAAAAD47SxbZHZIBAJ4ncYTD4ocrHMIu',
            'response': data['token'],
        }
        response = json.loads(requests.post('https://www.google.com/recaptcha/api/siteverify', json=body).text)
        return Response({'success': response['success']})