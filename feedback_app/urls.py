from django.urls import path

from feedback_app.views import SendFeedback, VerifyCaptcha

urlpatterns = [
    path('', SendFeedback.as_view()),
    path('verify', VerifyCaptcha.as_view())
]