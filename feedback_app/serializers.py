from rest_framework import serializers


class FedbackSerializer(serializers.Serializer):
    name = serializers.CharField()
    phone = serializers.CharField()
    message = serializers.CharField()
