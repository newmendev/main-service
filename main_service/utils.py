import os

from django.core.exceptions import ImproperlyConfigured


def get_env_variable_value(env_variable_name):
    '''Return env variable value or raise error if
    the variable is not defined

    '''
    try:
        return os.environ[env_variable_name]
    except KeyError:
        error_message = f'Set the {env_variable_name} environment variable'

        raise ImproperlyConfigured(error_message)
