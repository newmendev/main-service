from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('grappelli/', include('grappelli.urls')),
    path('123admin/', admin.site.urls),
    path('base_info/', include('info_app.urls')),
    path('projects/', include('projects_app.urls')),
    path('news/', include('news_app.urls')),
    path('send_feedback/', include('feedback_app.urls')),
    path('releases/', include('release_app.urls'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
