from django.db import models


class News(models.Model):
    title_ru = models.CharField(max_length=255)
    title_en = models.CharField(max_length=255, blank=True, default='')
    text_ru = models.TextField()
    text_en = models.TextField(default='')
    pic = models.ImageField(upload_to='static/images/', default='static/images/news_default.svg')
    partner = models.ImageField(upload_to='static/images/', default=None, blank=True)
    data = models.DateField()
    news_tags = models.TextField(default='')
    is_hidden = models.BooleanField(default=False)
    is_main = models.BooleanField(default=False)
    title = models.TextField(default='')
    description = models.TextField(default='')
    keyword = models.TextField(default='')
    url_str = models.CharField(max_length=50, default='')

    def __str__(self):
        return self.title_ru

    class Meta:
        managed = True
        db_table = 'news'
