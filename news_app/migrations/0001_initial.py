# Generated by Django 3.2.2 on 2021-08-04 10:35

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title_ru', models.CharField(max_length=255)),
                ('title_en', models.CharField(blank=True, default='', max_length=255)),
                ('text_ru', models.TextField()),
                ('text_en', models.TextField(default='')),
                ('pic', models.ImageField(default='static/images/news_default.svg', upload_to='static/images/')),
                ('partner', models.ImageField(blank=True, default=None, upload_to='static/images/')),
                ('data', models.DateField()),
                ('news_tags', models.TextField(default='')),
                ('is_hidden', models.BooleanField(default=False)),
                ('is_main', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'news',
                'managed': True,
            },
        ),
    ]
