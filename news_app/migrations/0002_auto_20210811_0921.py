# Generated by Django 3.2.2 on 2021-08-11 09:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='description',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='news',
            name='title',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='news',
            name='url_str',
            field=models.CharField(default='', max_length=50),
        ),
    ]
