from rest_framework import serializers

from .models import News


class NewsSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    text = serializers.SerializerMethodField()
    partner_url = serializers.SerializerMethodField()
    pic_url = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()

    seo_title = serializers.SerializerMethodField('get_seo_title')
    seo_description = serializers.SerializerMethodField('get_seo_description')
    seo_keyword = serializers.SerializerMethodField('get_seo_keyword')

    class Meta:
        model = News
        fields = ['id', 'title', 'text', 'pic_url', 'partner_url', 'data', 'tags',
                  'seo_title', 'seo_description', 'seo_keyword'
                  ]

    def get_id(self, post):
        if post.url_str:
            return post.url_str
        else:
            return post.id

    def get_title(self, post):
        if 'request' in self.context and self.context['request'].GET.get('lang') == 'en':
            return post.title_en
        else:
            return post.title_ru

    def get_text(self, post):
        if 'request' in self.context and self.context['request'].GET.get('lang') == 'en':
            return post.text_en.replace('/media/uploads', '')
        else:
            return post.text_ru.replace('/media/uploads', '')

    def get_partner_url(self, post):
        try:
            return post.partner.url.replace('static/images/', '')
        except ValueError:
            return None

    def get_pic_url(self, post):
        return post.pic.url.replace('static/images/', '')

    def get_tags(self, post):
        if post.news_tags is not None or post.news_tags != '':
            tags = ['#' + i for i in post.news_tags.split('#')]
            del tags[0]
            return tags
        else:
            return []

    def get_seo_title(self, post):
        return post.title

    def get_seo_description(self, post):
        return post.description

    def get_seo_keyword(self, post):
        return post.keyword
