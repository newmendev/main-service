from django.urls import path
from .views import GetNews, GetNewsDetail

urlpatterns = [
    path('', GetNews.as_view()),
    path('<slug:news_link>', GetNewsDetail.as_view()),
]