from django.shortcuts import get_object_or_404
from rest_framework import mixins, viewsets, generics
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from .serializers import NewsSerializer
from .models import News


class MinResultsSetPagination(PageNumberPagination):
    page_size = 12
    page_size_query_param = 'page_size'
    max_page_size = 12


class GetNews(generics.ListAPIView):
    serializer_class = NewsSerializer
    pagination_class = MinResultsSetPagination

    def get_queryset(self):
        if self.request.GET.get('is_main') is not None:
            return News.objects.order_by('-data').all().filter(is_hidden=False, is_main=True).order_by('-data')[:3]
        elif self.request.GET.get('tag') is not None:
            return News.objects.order_by('-data').all().filter(news_tags__contains=self.request.GET.get('tag'))
        else:
            return News.objects.order_by('-data', '-id').all().filter(is_hidden=False)

    def get(self, request, *args, **kwargs):
        response = self.list(request, *args, **kwargs)
        return Response(response.data)


class GetNewsDetail(generics.RetrieveUpdateDestroyAPIView):
    def get(self, request, news_link):
        try:
            video = get_object_or_404(News, pk=int(news_link))
        except:
            video = get_object_or_404(News, url_str=news_link)

        serializer = NewsSerializer(video, context={'request': self.request}, many=False)
        return Response(serializer.data)