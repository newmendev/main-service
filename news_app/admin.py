from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import News

class NewsAdmin(admin.ModelAdmin):
    fields = ['title_ru', 'title_en', 'text_ru', 'text_en', 'pic']

    class Meta:
        verbose_name_plural = _("Новости")


admin.site.register(News, NewsAdmin)
