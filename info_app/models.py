from django.db import models

class BaseInfo(models.Model):
    title_ru = models.CharField(max_length=400)
    title_en = models.CharField(max_length=400)
    address_ru = models.CharField(max_length=200)
    address_en = models.CharField(max_length=200)
    youtube = models.URLField()
    instagram = models.URLField()
    about_ru = models.TextField()
    about_en = models.TextField()
    about_additional_ru = models.TextField(default='')
    about_additional_en = models.TextField(default='')
    phone = models.CharField(max_length=30, default='')
    email = models.CharField(max_length=200, default='')
    video_url = models.CharField(max_length=50, default='')
    show_video = models.BooleanField(default=True)

    def __str__(self):
        return self.title_ru

    class Meta:
        managed = True
        db_table = 'base_info'


class Team(models.Model):
    photo = models.ImageField(upload_to='static/images/', default='static/images/user_default.svg')
    name_ru = models.CharField(max_length=255)
    name_en = models.CharField(max_length=255)
    position_ru = models.CharField(max_length=255)
    position_en = models.CharField(max_length=255)
    details_ru = models.TextField()
    details_en = models.TextField()
    is_trustee = models.BooleanField(default=False)
    priority = models.IntegerField(default=1)

    def __str__(self):
        return self.name_ru

    class Meta:
        managed = True
        db_table = 'team'


class Competence(models.Model):
    content_ru = models.CharField(max_length=255)
    content_en = models.CharField(max_length=255)
    priority = models.IntegerField(unique=True)

    class Meta:
        managed = True
        db_table = 'competence'


class Partners(models.Model):
    img = models.ImageField()
    text = models.CharField(max_length=255)
    url = models.URLField()
    priority = models.IntegerField(unique=True, default=None, null=True)

    class Meta:
        managed = True
        db_table = 'partners'


class Documents(models.Model):
    document = models.FileField()
    text = models.CharField(max_length=50)
    priority = models.IntegerField(unique=True)
    is_show = models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = 'documents'
