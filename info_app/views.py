from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import BaseInfo, Competence, Partners, Team, Documents
from projects_app.models import Projects
from news_app.models import News


class GetInfo(generics.GenericAPIView):
    def get(self, request):
        lang = request.GET.get('lang')

        data = {
            'title': '',
            'address': '',
            'youtube': '',
            'instagram': '',
            'about': '',
            'phone': '',
            'email': '',
            'video_url': '',
            'show_video': False,
            'competence': [],
            'partners': [],
            'teams': [],
            'trustee': [],
            'documents': []
        }

        base_info = BaseInfo.objects.get(pk=1)

        data['youtube'] = base_info.youtube
        data['instagram'] = base_info.instagram
        data['phone'] = base_info.phone
        data['email'] = base_info.email
        data['video_url'] = base_info.video_url
        data['show_video'] = base_info.show_video

        if lang == 'en':
            data['title'] = base_info.title_en
            data['address'] = base_info.address_en
            data['about'] = base_info.about_en
            data['about_additional_en'] = base_info.about_additional_en
            for c in Competence.objects.order_by('priority').all():
                data['competence'].append(c.content_en)
        else:
            data['title'] = base_info.title_ru
            data['address'] = base_info.address_ru
            data['about'] = base_info.about_ru
            data['about_additional_ru'] = base_info.about_additional_ru
            for c in Competence.objects.order_by('priority').all():
                data['competence'].append(c.content_ru)

        team = Team.objects.order_by('priority').all()
        for i in team:
            temp = {
                'photo': str(i.photo.url).replace('/static/images', ''),
                'name': '',
                'position': '',
                'details': ''
            }

            if lang == 'ru':
                temp['name'] = i.name_ru
                temp['position'] = i.position_ru
                temp['details'] = i.details_ru
            else:
                temp['name'] = i.name_en
                temp['position'] = i.position_en
                temp['details'] = i.details_en

            if i.is_trustee:
                data['trustee'].append(temp)
            else:
                data['teams'].append(temp)

        for p in Partners.objects.order_by('priority').all():
            data['partners'].append(
                {
                    'img': p.img.url.replace('static/images/', ''),
                    'text': p.text,
                    'url': p.url,
                }
            )

        for d in Documents.objects.order_by('priority').filter(is_show=True):
            data['documents'].append(
                {
                    'text': d.text,
                    'doc': d.document.url.replace('static/documents/', '')
                }
            )

        return Response(data)


class GetRoutes(APIView):
    def get(self, request):
        routes = {
            'news': [],
            'projects': [],
        }

        for i in Projects.objects.order_by('-id').all():
            if i.url_str != '':
                routes['projects'].append(i.url_str)
            else:
                routes['projects'].append(i.id)

        for i in News.objects.order_by('-id').all():
            if i.url_str != '':
                routes['news'].append(i.url_str)
            else:
                routes['news'].append(i.id)

        return Response(routes)
