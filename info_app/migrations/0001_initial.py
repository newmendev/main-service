# Generated by Django 3.2.2 on 2021-08-04 10:35

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BaseInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title_ru', models.CharField(max_length=400)),
                ('title_en', models.CharField(max_length=400)),
                ('address_ru', models.CharField(max_length=200)),
                ('address_en', models.CharField(max_length=200)),
                ('youtube', models.URLField()),
                ('instagram', models.URLField()),
                ('about_ru', models.TextField()),
                ('about_en', models.TextField()),
                ('about_additional_ru', models.TextField(default='')),
                ('about_additional_en', models.TextField(default='')),
                ('phone', models.CharField(default='', max_length=30)),
            ],
            options={
                'db_table': 'base_info',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(default='static/images/user_default.svg', upload_to='static/images/')),
                ('name_ru', models.CharField(max_length=255)),
                ('name_en', models.CharField(max_length=255)),
                ('position_ru', models.CharField(max_length=255)),
                ('position_en', models.CharField(max_length=255)),
                ('details_ru', models.TextField()),
                ('details_en', models.TextField()),
                ('is_trustee', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'team',
                'managed': True,
            },
        ),
    ]
