# Generated by Django 3.2.2 on 2021-08-10 10:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('info_app', '0002_auto_20210806_1104'),
    ]

    operations = [
        migrations.AddField(
            model_name='partners',
            name='priority',
            field=models.IntegerField(default=None, null=True, unique=True),
        ),
    ]
