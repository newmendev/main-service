from django.urls import path
from .views import GetInfo, GetRoutes

urlpatterns = [
    path('', GetInfo.as_view()),
    path('get_seo', GetRoutes.as_view()),
]