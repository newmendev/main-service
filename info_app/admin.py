from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import BaseInfo, Team


class BaseInfoAdmin(admin.ModelAdmin):
    fields = ['title_ru', 'title_en', 'address_ru', 'address_en', 'youtube', 'instagram', 'about_ru', 'about_en']

    class Meta:
        verbose_name_plural = _("Базовая информация")

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return True

    def has_delete_permission(self, request, obj=None):
        return False

class TeamAdmin(admin.ModelAdmin):
    fields = ['photo', 'name_ru', 'name_en', 'position_ru', 'position_en', 'details_ru', 'details_en']

    class Meta:
        verbose_name_plural = _("Команда")


admin.site.register(BaseInfo, BaseInfoAdmin)
admin.site.register(Team, TeamAdmin)
