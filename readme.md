# Основной севрсис для SVOE
Сервис написан на Python/Django
## Начало установки
### Stack
- Python 3.8
- Django 3.2.2
Перед началом установки проекта в системе должен быть установлен Python
## Установка
### 1. Перейти в папку проекта
*Это папка с файлом `manage.py`
Все дальшейшие действия совершаются внутри неё*
### 2. Создать и активировать виртуальное окружение
```
python -m venv venv
source venv/bin/activate
```
*venv - путь к папке виртуального окружения
можно оставить просто venv, и тогда папка создастся в текущей*
### 3. Установить зависимости
```
pip install -r requirements.txt
```
### 4. Добавить переменные окружения
```
source .env
```
**ЗАМЕЧАНИЕ:** 
* SECRET_KEY:  django-insecure-lnu^4bvi0w7rf#_9u-*(f=spduyes=ztbw6meq+-_54p#1^ryw (секретный ключ django)
* DEBUG: Режим дебага
* ALLOWED_HOSTS: Разрешенные хосты
* DB_NAME: имя БД 
* DB_USER: имя пользователя БД
* DB_PASSWORD: пароль пользователя БД
* DB_HOST: хост БД 
* DB_PORT: порт БД 
* EMAIL_HOST: Хост для email
* EMAIL_HOST_USER: Логин для почты
* EMAIL_HOST_PASSWORD: Пароль для почты
* EMAIL_PORT: Порт почты
* EMAIL_USE_TLS: Использование TLS (True или False)
* SEND_TO_FEED_BACK: почта для отправки обратной связи (d.yashin@bsl.dev) 
* STATIC: Путь до папки media/static в админ сервисе

### 5. Миграции
```
python manage.py makemigrations 
python manage.py migrate
```
### 6. Стартовать Gunicorn(Python WSGI HTTP Server) из папки проекта
 gunicorn --bind 0.0.0.0:$PORT main_service.wsgi
 
 Конфиг gunicorn лежит в папке main_service/gunicorn.py

