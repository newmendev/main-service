from django.shortcuts import get_object_or_404
from rest_framework import mixins, viewsets, generics
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from .serializers import *
from .models import *


class GetReleases(generics.ListAPIView):
    serializer_class = ReleasesSerializer

    def get_queryset(self):
        farm_types_filter = self.request.GET.get('farm_types') or None
        release_slug_filter = self.request.GET.get('release_slug') or None

        if farm_types_filter is not None:
            try:
                return Releases.objects.filter(
                    release_types__in=farm_types_filter.split(',')). \
                    order_by('-date')
            except Exception as e:
                print(e)
                return Releases.objects.order_by('-date').all()
        if release_slug_filter is not None:
            try:
                return Releases.objects.filter(pk=int(release_slug_filter))
            except Exception as e:
                print(e)
                try:
                    return Releases.objects.filter(url_str=release_slug_filter)
                except Exception as e:
                    print(e)
                    return Releases.objects.order_by('-date').all()

        return Releases.objects.order_by('-date').all()

    def get(self, request, *args, **kwargs):
        response = self.list(request, *args, **kwargs)
        return Response(response.data)


class GetFarmTypes(generics.ListAPIView):
    serializer_class = FarmTypesSerializer

    def get_queryset(self):
        return FarmTypes.objects.all()

    def get(self, request, *args, **kwargs):
        response = self.list(request, *args, **kwargs)
        return Response(response.data)
