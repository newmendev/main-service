from django.urls import path

from .views import GetReleases, GetFarmTypes

urlpatterns = [
    path('', GetReleases.as_view()),
    path('farm_types/', GetFarmTypes.as_view()),
]
