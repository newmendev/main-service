import datetime

from django.db import models


class FarmTypes(models.Model):
    name_ru = models.CharField(max_length=255, verbose_name='Имя на русском')
    name_en = models.CharField(max_length=255, default='',
                               verbose_name='Имя на английском')

    def __str__(self):
        return self.name_ru

    class Meta:
        managed = True
        db_table = 'farm_types'
        verbose_name = "Занятие ферм"
        verbose_name_plural = "Занятия ферм"


class Releases(models.Model):
    title_ru = models.CharField(max_length=255,
                                verbose_name='Заголовок на русском')
    title_en = models.CharField(max_length=255, blank=True, default='',
                                verbose_name='Заголовок на английском')
    video_url = models.TextField(blank=True, default='',
                                 verbose_name='Ссылка на видео')
    video_preview_pic = models.ImageField(upload_to='static/images/',
                                          verbose_name='Превью')
    release_types = models.ManyToManyField(FarmTypes, blank=True,
                                           verbose_name='Занятия фермы')

    # Основной текст
    text_ru = models.TextField(verbose_name='Текст на русском')
    text_en = models.TextField(default='', verbose_name='Текст на английском')

    # Личная история
    history_hero_pic = models.ImageField(upload_to='static/images/',
                                         verbose_name='Фото героя')
    history_hero_name_ru = models.CharField(max_length=255, blank=True,
                                            verbose_name='Имя героя на русском',
                                            default='')
    history_hero_name_en = models.CharField(max_length=255, blank=True,
                                            verbose_name='Имя героя на английском',
                                            default='')
    history_hero_post_ru = models.CharField(max_length=255, blank=True,
                                            verbose_name='Должность героя на русском',
                                            default='')
    history_hero_post_en = models.CharField(max_length=255, blank=True,
                                            verbose_name='Должность нероя на английском',
                                            default='')
    history_hero_text_ru = models.TextField(default='',
                                            verbose_name='Текст героя на русском')
    history_hero_text_en = models.TextField(default='', blank=True,
                                            verbose_name='Текст героя на английском')

    # Геолокация и контакты
    map_lat = models.CharField(max_length=255, verbose_name='Долгота')
    map_lng = models.CharField(max_length=255, verbose_name='Широта')
    address_name = models.CharField(max_length=255, default='',
                                    verbose_name='Имя адреса')
    address_url = models.CharField(max_length=255, blank=True, default='',
                                   verbose_name='Ссылка на адрес (url)(клик по адресу)')
    contact_text_ru = models.TextField(
        verbose_name='Блок контактов на русском')
    contact_text_en = models.TextField(default='', blank=True,
                                       verbose_name='Блок контактов на английском')
    farm_site_url = models.URLField(blank=True, default='',
                                    verbose_name='Ссылка на сайт')
    farm_instagram_url = models.URLField(blank=True, default='',
                                         verbose_name='Ссылка на instagram')
    farm_vk_url = models.URLField(blank=True, default='',
                                  verbose_name='Ссылка на VK')
    farm_fb_url = models.URLField(blank=True, default='',
                                  verbose_name='Ссылка на FB')

    # Базовые поля
    date = models.DateField(default=datetime.date.today,
                            verbose_name='Дата выпуска')
    url_str = models.CharField(max_length=50, default='', blank=True,
                               verbose_name='URL выпуска')

    # Значок
    badge_enabled = models.BooleanField(default=False, blank=False,
                                        verbose_name='Значок')

    def __str__(self):
        return self.title_ru

    class Meta:
        managed = True
        db_table = 'releases'
        verbose_name = "Выпуск"
        verbose_name_plural = "Выпуски"


class Product(models.Model):
    release = models.ForeignKey(Releases, on_delete=models.DO_NOTHING,
                                verbose_name='Выпуск',
                                related_name='popular_products')
    name_ru = models.CharField(max_length=255,
                               verbose_name='Название на русском')
    name_en = models.CharField(max_length=255, default='',
                               verbose_name='Название на английском')
    url_to_shop = models.CharField(max_length=255, default='',
                                   verbose_name='Ссылка в магазин (url)')
    pic = models.ImageField(upload_to='static/images/',
                            verbose_name='Фото')

    def __str__(self):
        return self.name_ru

    class Meta:
        managed = True
        db_table = 'farm_products'
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"


class ReleaseImage(models.Model):
    release = models.ForeignKey(Releases, on_delete=models.DO_NOTHING,
                                verbose_name='Выпуск', related_name='images')
    pic = models.ImageField(upload_to='static/images/', verbose_name='Фото')

    class Meta:
        managed = True
        db_table = 'farm_images'
        verbose_name = "Фото выпуска"
        verbose_name_plural = "Фото выпуска"
