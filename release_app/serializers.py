from rest_framework import serializers

from .models import *


class FarmTypesSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()

    class Meta:
        model = FarmTypes
        fields = ['id', 'name']

    def get_id(self, farm_type: FarmTypes):
        return farm_type.id

    def get_name(self, farm_type: FarmTypes):
        if 'request' in self.context and self.context['request'].GET.get(
                'lang') == 'en':
            return farm_type.name_en
        else:
            return farm_type.name_ru


class ProductSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    url_to_shop = serializers.SerializerMethodField()
    pic = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = ['name', 'url_to_shop', 'pic']

    def get_url_to_shop(self, farm_type: Product):
        return farm_type.url_to_shop

    def get_pic(self, farm_type: Product):
        return farm_type.pic.url.replace('static/images/', '')

    def get_name(self, farm_type: Product):
        if 'request' in self.context and self.context['request'].GET.get(
                'lang') == 'en':
            return farm_type.name_en
        else:
            return farm_type.name_ru


class ReleaseImageSerializer(serializers.ModelSerializer):
    pic = serializers.SerializerMethodField()

    class Meta:
        model = ReleaseImage
        fields = ['pic']

    def get_pic(self, farm_type: ReleaseImage):
        return farm_type.pic.url.replace('static/images/', '')


class ReleasesSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    video_url = serializers.SerializerMethodField()
    video_preview_pic = serializers.SerializerMethodField()
    release_types = FarmTypesSerializer(many=True)
    text = serializers.SerializerMethodField()
    history_hero_pic = serializers.SerializerMethodField()
    history_hero_name = serializers.SerializerMethodField()
    history_hero_post = serializers.SerializerMethodField()
    history_hero_text = serializers.SerializerMethodField()
    map_lat = serializers.SerializerMethodField()
    map_lng = serializers.SerializerMethodField()
    address_name = serializers.SerializerMethodField()
    address_url = serializers.SerializerMethodField()
    contact_text = serializers.SerializerMethodField()
    farm_site_url = serializers.SerializerMethodField()
    farm_instagram_url = serializers.SerializerMethodField()
    farm_vk_url = serializers.SerializerMethodField()
    farm_fb_url = serializers.SerializerMethodField()
    date = serializers.SerializerMethodField()
    badge_enabled = serializers.SerializerMethodField()
    popular_products = ProductSerializer(many=True)
    images = ReleaseImageSerializer(many=True)
    url_str = serializers.SerializerMethodField()

    class Meta:
        model = Releases
        fields = ['id', 'title', 'video_url',
                  'video_preview_pic', 'release_types', 'text',
                  'history_hero_pic', 'history_hero_name', 'history_hero_post',
                  'history_hero_text', 'map_lat', 'map_lng', 'address_name',
                  'address_url', 'contact_text', 'contact_text', 'farm_fb_url',
                  'farm_site_url', 'farm_instagram_url', 'farm_vk_url', 'date',
                  'badge_enabled', 'popular_products', 'images', 'url_str']

    def get_id(self, release: Releases):
        return release.id

    def get_title(self, release: Releases):
        if 'request' in self.context and self.context['request'].GET.get(
                'lang') == 'en':
            return release.title_en
        else:
            return release.title_ru

    def get_video_url(self, release: Releases):
        return release.video_url

    def get_video_preview_pic(self, release: Releases):
        return release.video_preview_pic.url.replace('static/images/', '')

    def get_release_types(self, release: Releases):
        return release.release_types

    def get_text(self, release: Releases):
        if 'request' in self.context and self.context['request'].GET.get(
                'lang') == 'en':
            return release.text_en.replace('/media/uploads', '')
        else:
            return release.text_ru.replace('/media/uploads', '')

    def get_history_hero_pic(self, release: Releases):
        return release.history_hero_pic.url.replace('static/images/', '')

    def get_history_hero_name(self, release: Releases):
        if 'request' in self.context and self.context['request'].GET.get(
                'lang') == 'en':
            return release.history_hero_name_en
        else:
            return release.history_hero_name_ru

    def get_history_hero_post(self, release: Releases):
        if 'request' in self.context and self.context['request'].GET.get(
                'lang') == 'en':
            return release.history_hero_post_en
        else:
            return release.history_hero_post_ru

    def get_history_hero_text(self, release: Releases):
        if 'request' in self.context and self.context['request'].GET.get(
                'lang') == 'en':
            return release.history_hero_text_en
        else:
            return release.history_hero_text_ru

    def get_map_lat(self, release: Releases):
        return release.map_lat

    def get_map_lng(self, release: Releases):
        return release.map_lng

    def get_address_name(self, release: Releases):
        return release.address_name

    def get_address_url(self, release: Releases):
        return release.address_url

    def get_contact_text(self, release: Releases):
        if 'request' in self.context and self.context['request'].GET.get(
                'lang') == 'en':
            return release.contact_text_en
        else:
            return release.contact_text_en

    def get_farm_site_url(self, release: Releases):
        return release.farm_site_url

    def get_farm_instagram_url(self, release: Releases):
        return release.farm_instagram_url

    def get_farm_vk_url(self, release: Releases):
        return release.farm_vk_url

    def get_farm_fb_url(self, release: Releases):
        return release.farm_fb_url

    def get_date(self, release: Releases):
        return release.date

    def get_badge_enabled(self, release: Releases):
        return release.badge_enabled

    def get_popular_products(self, release: Releases):
        return release.popular_products

    def get_images(self, release: Releases):
        return release.images

    def get_url_str(self, release: Releases):
        return release.url_str
