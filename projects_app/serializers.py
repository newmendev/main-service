from rest_framework import serializers

from .models import ProjectTypes, MediaTypes, Projects, Videos


class ProjectTypesSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()
    class Meta:
        model = ProjectTypes
        fields = ['id', 'type']

    def get_type(self, type):
        if self.context['request'].GET.get('lang') == 'en':
            return type.name_en
        else:
            return type.name_ru


class MediaTypesSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()
    class Meta:
        model = MediaTypes
        fields = ['id', 'type']

    def get_type(self, type):
        if 'request' in self.context and self.context['request'].GET.get('lang') == 'en':
            return type.name_en
        else:
            return type.name_ru


class ProjectSerializer(serializers.ModelSerializer):
    title = serializers.SerializerMethodField()
    text = serializers.SerializerMethodField()
    pic_url = serializers.SerializerMethodField()
    id = serializers.SerializerMethodField()

    seo_title = serializers.SerializerMethodField('get_seo_title')
    seo_description = serializers.SerializerMethodField('get_seo_description')
    seo_keyword = serializers.SerializerMethodField('get_seo_keyword')

    class Meta:
        model = Projects
        id = serializers.SerializerMethodField()
        fields = ['id', 'project_type', 'title', 'text', 'pic_url', 'big_block', 'content', 'data',
                  'seo_title', 'seo_description', 'seo_keyword']

    def get_title(self, project):
        if 'request' in self.context and self.context['request'].GET.get('lang') == 'en':
            return project.title_en
        else:
            return project.title_ru

    def get_text(self, project):
        if 'request' in self.context and self.context['request'].GET.get('lang') == 'en':
            return project.text_en.replace('/media/uploads', '')
        else:
            return project.text_ru.replace('/media/uploads', '')

    def get_pic_url(self, project):
        return project.pic.url.replace('static/images/', '')

    def get_id(self, project):
        if project.url_str:
            return project.url_str
        else:
            return project.id

    def get_seo_title(self, project):
        return project.title


    def get_seo_description(self, project):
        return project.description


    def get_seo_keyword(self, project):
        return project.keyword


class VideoSerializer(serializers.ModelSerializer):
    title = serializers.SerializerMethodField()
    preview_url = serializers.SerializerMethodField()
    id = serializers.SerializerMethodField()

    class Meta:
        model = Videos
        fields = ['id', 'project_type', 'title', 'url', 'preview_url', 'date', 'big_block']

    def get_id(self, video):
        if video.url_str:
            return video.url_str
        else:
            return video.id

    def get_title(self, video):
        if 'request' in self.context and self.context['request'].GET.get('lang') == 'en':
            return video.title_en
        else:
            return video.title_ru

    def get_preview_url(self, video):
        return video.preview.url.replace('static/images/', '')
