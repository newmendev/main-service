import datetime

from django.db import models


class ProjectTypes(models.Model):
    name_ru = models.CharField(max_length=255)
    name_en = models.CharField(max_length=255, default='')
    priority = models.IntegerField(unique=True, default=None, null=True)

    def __str__(self):
        return self.name_ru

    class Meta:
        managed = True
        db_table = 'project_types'


class MediaTypes(models.Model):
    name_ru = models.CharField(max_length=255)
    name_en = models.CharField(max_length=255, default='')
    priority = models.IntegerField(unique=True, default=None, null=True)

    def __str__(self):
        return self.name_ru

    class Meta:
        managed = True
        db_table = 'media_types'


class Videos(models.Model):
    project_type = models.ForeignKey(MediaTypes, on_delete=models.DO_NOTHING, default=None)
    title_ru = models.CharField(max_length=255)
    title_en = models.CharField(max_length=255, default='')
    url = models.URLField()
    preview = models.ImageField()
    date = models.DateField()
    big_block = models.BooleanField(default=False)
    title = models.TextField(default='')
    description = models.TextField(default='')
    url_str = models.CharField(max_length=50, default='')

    def __str__(self):
        return self.title_ru

    class Meta:
        managed = True
        db_table = 'videos'


class Projects(models.Model):
    project_type = models.ForeignKey(ProjectTypes, on_delete=models.DO_NOTHING)
    title_ru = models.CharField(max_length=255)
    title_en = models.CharField(max_length=255, blank=True, default='')
    text_ru = models.TextField()
    text_en = models.TextField(default='')
    pic = models.ImageField(upload_to='static/images/', default='static/images/project_default.svg')
    big_block = models.BooleanField(default=False)
    content = models.TextField()
    data = models.DateField(default=datetime.date.today)
    title = models.TextField(default='')
    description = models.TextField(default='')
    keyword = models.TextField(default='')
    url_str = models.CharField(max_length=50, default='')

    def __str__(self):
        return self.title_ru

    class Meta:
        managed = True
        db_table = 'projects'
