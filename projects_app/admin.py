from django.contrib import admin

from .models import ProjectTypes, Videos, Projects


class ProjectTypesAdmin(admin.ModelAdmin):
    fields = ['name_ru', 'name_en']


class VideosAdmin(admin.ModelAdmin):
    fields = ['project_type', 'title_ru', 'title_en', 'url', 'date', 'big_block']


class ProjectsAdmin(admin.ModelAdmin):
    fields = ['project_type', 'title_ru', 'title_en', 'text_ru', 'text_en', 'pic', 'big_block', 'content']


admin.site.register(ProjectTypes, ProjectTypesAdmin)
admin.site.register(Videos, VideosAdmin)
admin.site.register(Projects, ProjectsAdmin)