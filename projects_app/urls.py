from django.urls import path
from .views import GetProjectTypes, GetMediaTypes, GetProjects, GetVideos, GetProjectDetail, GetViedDetail


urlpatterns = [
    path('', GetProjects.as_view()),
    path('videos', GetVideos.as_view()),
    path('videos/<slug:project_link>', GetViedDetail.as_view()),
    path('project_types', GetProjectTypes.as_view()),
    path('media_types', GetMediaTypes.as_view()),
    path('<slug:project_link>', GetProjectDetail.as_view()),

]
