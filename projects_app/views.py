from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework import mixins, viewsets, generics
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import ProjectTypesSerializer, MediaTypesSerializer, ProjectSerializer, VideoSerializer
from .models import ProjectTypes, MediaTypes, Projects, Videos


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 8
    page_size_query_param = 'page_size'
    max_page_size = 12


class GetVideos(generics.ListAPIView):
    serializer_class = VideoSerializer
    pagination_class = StandardResultsSetPagination

    filter = {
        'Test': ''
    }

    def get_queryset(self):
        if self.request.GET.get('type_id'):
            return Videos.objects.order_by('-date', '-id').filter(project_type=self.request.GET.get('type_id'))
        else:
            return Videos.objects.order_by('-date', '-id').all()

    def get(self, request, *args, **kwargs):
        response = self.list(request, *args, **kwargs)
        return Response(response.data)


class GetViedDetail(generics.RetrieveUpdateDestroyAPIView):
    def get(self, request, project_link):
        try:
            video = get_object_or_404(Videos, pk=int(project_link))
        except:
            video = get_object_or_404(Videos, url_str=project_link)

        serializer = VideoSerializer(video, many=False)
        return Response(serializer.data)


class GetProjectTypes(generics.ListAPIView):
    serializer_class = ProjectTypesSerializer

    def get_queryset(self):
        return ProjectTypes.objects.all().order_by('priority')

    def get(self, request, *args, **kwargs):
        response = self.list(request, *args, **kwargs)
        return Response(response.data)


class GetMediaTypes(generics.ListAPIView):
    serializer_class = MediaTypesSerializer

    def get_queryset(self):
        return MediaTypes.objects.all().order_by('priority')

    def get(self, request, *args, **kwargs):
        response = self.list(request, *args, **kwargs)
        return Response(response.data)


def insert_to_list(data, item, is_even):
    for idx in range(len(data)):
        if is_even:
            if data[idx] != 2:
                if isinstance(data[idx], int):
                    item['big_block'] = False
                    data[idx] = item
                    break
        else:
            if data[idx] != 0:
                if isinstance(data[idx], int):
                    item['big_block'] = False
                    data[idx] = item
                    break


class GetProjects(generics.ListAPIView):
    serializer_class = ProjectSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        if self.request.GET.get('type_id'):
            return Projects.objects.order_by('-data', '-id').filter(project_type_id=self.request.GET.get('type_id'))
        else:
            return Projects.objects.all().order_by('-data', '-id')

    def get(self, request, *args, **kwargs):
        response = self.list(request, *args, **kwargs)
        data = list(range(0, len(response.data['results'])))
        is_even = False
        if request.GET.get('page'):
            if int(request.GET.get('page')) % 2 == 0:
                is_even = True
        is_big = False
        for r in response.data['results']:
            if r['big_block']:
                is_big = True
                break

        if is_big:
            if len(data) >= 3:
                for i in response.data['results']:
                    if i['big_block']:
                        if is_even and isinstance(data[2], int):
                            data[2] = i
                        elif not is_even and isinstance(data[0], int):
                            data[0] = i
                        else:
                            insert_to_list(data, i, is_even)
                    else:
                        insert_to_list(data, i, is_even)
            else:
                for i in response.data['results']:
                    insert_to_list(data, i, is_even)
        else:
            for idx, value in enumerate(response.data['results']):
                if idx == 2 and is_even:
                    value['big_block'] = True
                    data[2] = value
                elif idx == 0 and not is_even:
                    value['big_block'] = True
                    data[0] = value
                else:
                    insert_to_list(data, value, is_even)

        for i in range(len(data)):
            response.data['results'][i] = data[i]

        return Response(response.data)


class GetProjectDetail(APIView):
    def get(self, request, project_link):
        try:
            project = get_object_or_404(Projects, pk=int(project_link))
        except:
            project = get_object_or_404(Projects, url_str=project_link)
        serializer = ProjectSerializer(project, context={'request': self.request}, many=False)
        return Response(serializer.data)

